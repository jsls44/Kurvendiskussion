# Thema Kurvendiskussion für Polynome

## Team Christoph Kunze, Kai Kubsch, Jan Schlosser

## Funktionsbeschreibung
- GUI
- Plotter
- Polynomdivision 
- Ableiten
- Integration
- Nullstellen, Extrema, Wendepunkte berechnen
- Grenzwert (?)

## Bezug zu Mathematik
analytische/numerische Kurvendiskussion für Polynome

## Zeitplan
- Grobkonzept Mitte Oktober
- Feinkonzept Ende Oktober
- erste Version Ende November
- Testversion Mitte Dezember

## Aufgabenteilung
- Christoph: GUI, Plotter, Dokumentation
- Kai: Ableiten, Integration, Präsentation
- Jan: Polynomdivision, Nullstellen, Extrempunkte, Wendepunkte, Logo


## Zusammenarbeit
- Regelmäßige Treffen 14-tägig 
- Dokumente in gemeinsamer Cloud
- gemeinsame Whats App Gruppe

## Dokumentation
- Basierend auf Javadoc
- Sprache Deutsch


## Offene Punkte, Fragen
- Grenzwert (Zeit?)

